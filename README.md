# TestMultiTarget

Android Studio 測試不同app的皮使用同樣的java code和layout

## app流程&架構圖

![](./document/flowchart.png)

## app執行結果圖

### cFoto

![](./document/appshot_cfoto.png)

### pink-fun

![](./document/appshot_pinkfun.png)

## 開發教學

[AndroidStudio共享res教學.pdf](./document/AndroidStudio共享res教學.pdf)

## Author

Chih-chieh Chang, ccch.realtouch@gmail.com

## License

YoutubeParser is available under the MIT license. See the LICENSE file for more info.
