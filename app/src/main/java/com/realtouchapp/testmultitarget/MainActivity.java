package com.realtouchapp.testmultitarget;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import tools.ActivityHelper;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View btn_test_activity = findViewById(R.id.btn_test_activity);
        btn_test_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 從cfoto和pinkfun裡都有的tools.ActivityHelper這個類，來達成不同product flavor開啟不同畫面的Activity功能
                ActivityHelper.startTestActivity(MainActivity.this);
            }
        });

    }
}
