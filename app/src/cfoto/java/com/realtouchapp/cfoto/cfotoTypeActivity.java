package com.realtouchapp.cfoto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.realtouchapp.testmultitarget.R;

public class cfotoTypeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cfoto_type);

        View btn_open_activity_in_main = findViewById(R.id.btn_open_activity_in_main);
        btn_open_activity_in_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(cfotoTypeActivity.this, com.realtouchapp.testmultitarget.ThirdCallActivity.class));
            }
        });

    }
}
