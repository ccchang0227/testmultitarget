package tools;

import android.content.Context;
import android.content.Intent;

/**
 * Created by C.C.Chang on 2017/5/17.
 *
 * @version 1
 */

public final class ActivityHelper {

    public static void startTestActivity(Context context) {
        context.startActivity(new Intent(context, com.realtouchapp.cfoto.cfotoTypeActivity.class));
    }

}
